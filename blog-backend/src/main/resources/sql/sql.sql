
 Create table categories(
    id varchar,
    nom varchar NOT NULL,
    primary key (id)
 )

  Create table tags(
     id varchar,
     nom varchar NOT NULL,
     primary key (id)
  )
   Create table utilisateurs(
      id varchar,
      pseudo varchar,
      email varchar,
      password varchar,
      primary key (id)
    )


Create table articles(
 	id varchar NOT NULL,
 	statut varchar,
 	titre varchar NOT NULL,
 	contenu varchar NOT NULL,
 	resume varchar(50),
 	id_categorie varchar references categories(id),
 	id_utilisateur varchar references utilisateurs(id),
 	primary key (id)

 )

  Create table articles_tags(
    id varchar,
    id_tags varchar references tags(id),
    id_articles varchar references articles(id),
    primary key (id)
  )

