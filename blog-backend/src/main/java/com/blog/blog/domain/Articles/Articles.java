package com.blog.blog.domain.Articles;

import com.blog.blog.domain.Users.Utilisateurs;
import javax.persistence.*;

@Entity
@Table(name = "articles")
public class Articles {

    @Id
    String id;
    String titre;
    String contenu;
    String resume;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "statut")
    StatutArticles statut;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_utilisateur")
    private Utilisateurs utilisateurs;

    protected Articles() {
    }

    public Articles(String id,Utilisateurs utilisateurs, StatutArticles statut, String titre, String contenu, String resume) {
        this.id = id;
        this.utilisateurs = utilisateurs;
        this.titre = titre;
        this.contenu = contenu;
        this.resume = resume;
        this.statut =  statut;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StatutArticles getStatut() {
        return statut;
    }

    public void setStatut(StatutArticles statut) {
        this.statut = statut;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public Utilisateurs getUtilisateurs() {
        return utilisateurs;
    }

    public void setUtilisateurs(Utilisateurs utilisateurs) {
        this.utilisateurs = utilisateurs;
    }


}
