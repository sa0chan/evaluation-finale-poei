package com.blog.blog.domain.Users;

import com.blog.blog.domain.Articles.Articles;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "utilisateurs")
public class Utilisateurs {

    @Id
    String id;
    String pseudo;
    String email;
    String password;

    //@OneToMany(mappedBy = "articles")
    @OneToMany(mappedBy = "utilisateurs")

    List<Articles> articlesList;


    protected  Utilisateurs(){ }

    public Utilisateurs(String id, String pseudo, String email, String password){
        this.id  = id ;
        this.pseudo = pseudo;
        this.email =email;
        this.password =password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
