package com.blog.blog.domain.Articles;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Categories {

    @Id
    String id;
    String nom;


    public Categories() {}


    public Categories(String id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

}
