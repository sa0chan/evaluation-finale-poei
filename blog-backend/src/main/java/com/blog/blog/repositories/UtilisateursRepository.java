package com.blog.blog.repositories;

import com.blog.blog.domain.Users.Utilisateurs;
import org.springframework.data.repository.CrudRepository;
import java.util.Optional;

public interface UtilisateursRepository  extends CrudRepository<Utilisateurs, String> {
    //Optional<Utilisateurs> findByNom(String nom);
    Optional<Utilisateurs> findById(String id);
}

