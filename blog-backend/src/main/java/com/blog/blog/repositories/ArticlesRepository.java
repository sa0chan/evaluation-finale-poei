package com.blog.blog.repositories;

import com.blog.blog.domain.Articles.Articles;
import com.blog.blog.domain.Articles.StatutArticles;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ArticlesRepository extends CrudRepository<Articles, String> {
    List<Articles> findByTitre(String title);
    List<Articles> findByStatut(StatutArticles statut);
    Optional<Articles> findById(String id);

}
