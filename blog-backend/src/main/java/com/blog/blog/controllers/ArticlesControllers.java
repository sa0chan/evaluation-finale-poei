package com.blog.blog.controllers;

import com.blog.blog.domain.Articles.Articles;
import com.blog.blog.representation.Article.ArticlesRepresentation;
import com.blog.blog.representation.Article.ArticlesRepresentationMapper;
import com.blog.blog.services.ArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/articles")
public class ArticlesControllers {


        private ArticlesService service;
        private ArticlesRepresentationMapper mapper;


        @Autowired
        public ArticlesControllers(ArticlesService service, ArticlesRepresentationMapper mapper) {
            this.service = service;
            this.mapper = mapper;

        }

        @GetMapping
        List<ArticlesRepresentation> getAllArticles() {

            return this.service.getAllArticles().stream()
                    .map(this.mapper::mapArticleToArticleRepresentation)
                    .collect(Collectors.toList());
        }


        @GetMapping("/{id}")
        ResponseEntity<ArticlesRepresentation> getOne(@PathVariable("id") String id) {
            Optional<Articles> bloc = this.service.getOneArticle(id);

            return bloc
                    .map(this.mapper::mapArticleToArticleRepresentation)
                    .map(ResponseEntity::ok)
                    .orElseGet(() -> ResponseEntity.notFound().build());
        }


    @PostMapping
    ArticlesRepresentation createArticles(@RequestBody ArticlesRepresentation body){
        final Articles article;
        article = this.mapper.mapArticleRepresentationToArticle(body);
        this.service.createArticle(article);
        return this.mapper.mapArticleToArticleRepresentation(article);

    }


    @DeleteMapping("/{id}")
    void deletebyId(@PathVariable("id") String id) {
            this.service.deleteArticle(id);
        }

    @PutMapping("/{id}")
    public ResponseEntity<ArticlesRepresentation> editerArticle(@PathVariable("id") String id, @RequestBody ArticlesRepresentation body) {
        this.service.updateOneArticle(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }


    @PostMapping("/publication/{id}")
    public ResponseEntity<ArticlesRepresentation> publierArticle(@PathVariable("id") String id) {
        Optional<Articles> article = this.service.publicationArticle(id);
        return article
                .map(this.mapper::mapArticleToArticleRepresentation)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
