package com.blog.blog.controllers;

import com.blog.blog.domain.Users.Utilisateurs;
import com.blog.blog.representation.Utilisateur.UtilisateurRepresentation;
import com.blog.blog.representation.Utilisateur.UtilisateurRepresentationMapper;
import com.blog.blog.services.UtilisateursService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/utilisateurs")
public class UtilisateursControllers {


    private UtilisateursService service;
    private UtilisateurRepresentationMapper mapper;


    @Autowired
    public UtilisateursControllers(UtilisateursService service, UtilisateurRepresentationMapper mapper) {
        this.service = service;
        this.mapper = mapper;

    }

    @GetMapping
    List<UtilisateurRepresentation> getAllUtilisateurs() {

        return this.service.getAllUtilisateurs().stream()
                .map(this.mapper::mapUtilisateurToUtilisateurRepresentation)
                .collect(Collectors.toList());
    }


    @GetMapping("/{id}")
    ResponseEntity<UtilisateurRepresentation> getOneUtilisateur(@PathVariable("id") String id) {
        Optional<Utilisateurs> bloc = this.service.getOneUtilisateur(id);

        return bloc
                .map(this.mapper::mapUtilisateurToUtilisateurRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


    @PostMapping
    UtilisateurRepresentation createUtilisateur(@RequestBody UtilisateurRepresentation body){
        final Utilisateurs utilisateurs;
        utilisateurs = this.mapper.mapUtilisateurRepresentationToUtilisateur(body);
        this.service.createUtilisateur(utilisateurs);
        return this.mapper.mapUtilisateurToUtilisateurRepresentation(utilisateurs);

    }


    @DeleteMapping("/{id}")
    void deleteUtilisateurbyId(@PathVariable("id") String id) {
        this.service.deleteUtilisateur(id);
    }




}
