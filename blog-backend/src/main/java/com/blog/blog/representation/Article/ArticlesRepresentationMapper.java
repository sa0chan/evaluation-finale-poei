package com.blog.blog.representation.Article;

import com.blog.blog.domain.Articles.Articles;
import com.blog.blog.domain.Users.Utilisateurs;
import com.blog.blog.repositories.ArticlesRepository;
import com.blog.blog.repositories.UtilisateursRepository;
import org.springframework.stereotype.Component;

@Component
public class ArticlesRepresentationMapper {

    private ArticlesRepository repository;
    private UtilisateursRepository utilisateurRepository;



    public ArticlesRepresentationMapper(ArticlesRepository repository, UtilisateursRepository utilisateursRepository) {
        this.repository = repository;
        this.utilisateurRepository = utilisateursRepository;
    }

    public ArticlesRepresentation mapArticleToArticleRepresentation(Articles article) {

        ArticlesRepresentation result =
                new ArticlesRepresentation(article.getId(),article.getUtilisateurs().getId(),article.getStatut(), article.getTitre(), article.getContenu(), article.getResume());
        result.setId(article.getId());
        return result;
    }

    public Articles mapArticleRepresentationToArticle(ArticlesRepresentation articleRepresentation) {
        Utilisateurs utilisateur = utilisateurRepository.findById(articleRepresentation.getUtilisateursId()).orElseThrow();
        Articles result =
                new Articles(articleRepresentation.getId(),utilisateur,articleRepresentation.getStatutArticles(), articleRepresentation.getTitre(), articleRepresentation.getContenu(), articleRepresentation.getResume());
        return result;
    }
}

