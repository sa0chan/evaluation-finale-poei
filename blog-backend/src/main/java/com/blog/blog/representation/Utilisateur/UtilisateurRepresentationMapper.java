package com.blog.blog.representation.Utilisateur;
import com.blog.blog.domain.Users.Utilisateurs;
import com.blog.blog.repositories.UtilisateursRepository;
import org.springframework.stereotype.Component;

@Component

public class UtilisateurRepresentationMapper {

    private UtilisateursRepository repository;


    public UtilisateurRepresentationMapper(UtilisateursRepository repository) {
        this.repository = repository;
    }

    public UtilisateurRepresentation mapUtilisateurToUtilisateurRepresentation(Utilisateurs utilisateur) {

        UtilisateurRepresentation result =
                new UtilisateurRepresentation(utilisateur.getId(),utilisateur.getPseudo(),utilisateur.getEmail(),utilisateur.getPassword());
        result.setId(utilisateur.getId());
        return result;
    }

    public Utilisateurs mapUtilisateurRepresentationToUtilisateur(UtilisateurRepresentation utilisateurRepresentation) {
        Utilisateurs result =
                new Utilisateurs(utilisateurRepresentation.getId(),utilisateurRepresentation.getPseudo(), utilisateurRepresentation.getEmail(), utilisateurRepresentation.getPassword());
        return result;
    }
}
