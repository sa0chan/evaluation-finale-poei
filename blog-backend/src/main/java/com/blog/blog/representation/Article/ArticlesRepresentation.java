package com.blog.blog.representation.Article;

import com.blog.blog.domain.Articles.StatutArticles;

public class ArticlesRepresentation {

    String id;
    StatutArticles statutArticles;
    String titre;
    String contenu;
    String resume;
    String utilisateursId;

    public ArticlesRepresentation(String id, String utilisateursId, StatutArticles statutArticles, String titre, String contenu, String resume) {
        this.id = id;
        this.utilisateursId = utilisateursId;
        this.statutArticles = statutArticles;
        this.titre = titre;
        this.contenu = contenu;
        this.resume = resume;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StatutArticles getStatutArticles() {
        return statutArticles;
    }

    public void setStatutArticles(StatutArticles statutArticles) {
        this.statutArticles = statutArticles;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getUtilisateursId() {
        return utilisateursId;
    }

    public void setUtilisateursId(String utilisateurs) {
        this.utilisateursId = utilisateurs;
    }


}
