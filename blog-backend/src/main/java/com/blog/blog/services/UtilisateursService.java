package com.blog.blog.services;

import com.blog.blog.domain.Articles.Articles;
import com.blog.blog.domain.Articles.StatutArticles;
import com.blog.blog.domain.Users.Utilisateurs;
import com.blog.blog.repositories.ArticlesRepository;
import com.blog.blog.repositories.UtilisateursRepository;
import com.blog.blog.representation.Article.ArticlesRepresentation;
import com.blog.blog.representation.Utilisateur.UtilisateurRepresentation;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UtilisateursService {
    IdGenerateur idGenerator;
    private UtilisateursRepository repository;

    public UtilisateursService(IdGenerateur idGenerator, UtilisateursRepository repository) {
        this.idGenerator = idGenerator;
        this.repository = repository;
    }


    public List<Utilisateurs> getAllUtilisateurs() {
        List<Utilisateurs> userList = new ArrayList<>();
        repository.findAll().forEach(userList::add);
        return userList;
    }

    public Optional<Utilisateurs> getOneUtilisateur(String id) {
        return repository.findById(id);
    }

    //a mettre quand on sait que la methode peut échouer : par exemple pas d'autorisation de plus de 10 perso par player
    @Transactional(rollbackOn = Exception.class)
    public Utilisateurs createUtilisateur(Utilisateurs utilisateurs) {
        utilisateurs.setId(idGenerator.generateNewId());
        //ajouter un hashage de password
        repository.save(utilisateurs);
        return utilisateurs;
    }

    public void deleteUtilisateur(String id) {
        repository.deleteById(id);
    }



}
