package com.blog.blog.services;

import com.blog.blog.domain.Articles.Articles;
import com.blog.blog.domain.Articles.StatutArticles;
import com.blog.blog.domain.Users.Utilisateurs;
import com.blog.blog.repositories.ArticlesRepository;
import com.blog.blog.repositories.UtilisateursRepository;
import com.blog.blog.representation.Article.ArticlesRepresentation;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
public class ArticlesService {

    IdGenerateur idGenerator;
    private ArticlesRepository repository;
    private UtilisateursRepository utilisateursRepository;

    public ArticlesService(IdGenerateur idGenerator, ArticlesRepository repository, UtilisateursRepository utilisateursRepository) {
        this.idGenerator = idGenerator;
        this.repository = repository;
        this.utilisateursRepository =utilisateursRepository;
    }


    public List<Articles> getAllArticles() {
        List<Articles> userList = new ArrayList<>();
        repository.findAll().forEach(userList::add);
        return userList;
    }

    public Optional<Articles> getOneArticle(String id) {
        return repository.findById(id);
    }

    //a mettre quand on sait que la methode peut échouer : par exemple pas d'autorisation de plus de 10 perso par player
    @Transactional(rollbackOn = Exception.class)
    public Articles createArticle(Articles article) {
        article.setId(idGenerator.generateNewId());
        article.setStatut(StatutArticles.draft);
        repository.save(article);
        return article;
    }

    public void deleteArticle(String id) {
        repository.deleteById(id);
    }

    public Articles updateOneArticle(String id, ArticlesRepresentation body) {


            Utilisateurs utilisateurFind = utilisateursRepository.findById(body.getUtilisateursId()).orElseThrow();
            Articles articleAModifier = getOneArticle(id).orElseThrow();
            Articles articleDansBase = new Articles(this.idGenerator.generateNewId(),utilisateurFind, body.getStatutArticles(),body.getTitre(), body.getContenu(), body.getResume());


            StatutArticles statut = articleAModifier.getStatut();
            Utilisateurs utilisateur = utilisateursRepository.findById(body.getUtilisateursId()).orElseThrow();;
            String titre = "";
            String contenu = "";
            String resume = "";


            if (articleDansBase.getTitre().equals("")) {
                titre = articleAModifier.getTitre();
            } else {
                titre = articleDansBase.getTitre();
            }
            if (articleDansBase.getContenu().equals("")) {
                contenu = articleAModifier.getContenu();
            } else {
                contenu = articleDansBase.getContenu();
            }
            if (articleDansBase.getResume().equals("")) {
                resume = articleAModifier.getResume();
            } else {
                resume = articleDansBase.getResume();
            }

            Articles newArticle = new Articles(id,utilisateur, statut, titre, contenu, resume);
            return this.repository.save(newArticle);



    }

    public Optional<Articles> publicationArticle (String id) {
        Optional<Articles> articleAPublier = this.repository.findById(id);
        if (articleAPublier.isPresent()) {
            articleAPublier.get().setStatut(StatutArticles.published);
            articleAPublier.get().setId(articleAPublier.get().getId());
            articleAPublier.get().setTitre(articleAPublier.get().getTitre());
            articleAPublier.get().setResume(articleAPublier.get().getResume());
            articleAPublier.get().setContenu(articleAPublier.get().getContenu());
            articleAPublier.get().setUtilisateurs(articleAPublier.get().getUtilisateurs());
            this.repository.save(articleAPublier.get());
        } else {
            throw new IllegalArgumentException("L'ID renseigné n'est pas valide");
        }
        return articleAPublier;
    }



}
