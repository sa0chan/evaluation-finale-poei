import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BlogApiService} from '../../../services/BlogApi.service';
import {UtilisateursModel} from '../../../models/Utilisateurs/Utilisateurs.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  utilisateurList: UtilisateursModel[];
  /* @Output()
   utilisateurLogged = new EventEmitter<UtilisateursModel>();*/
  utilisateurLogged: UtilisateursModel;
  email: string;
  password: string;
  display = false;
  islog: boolean;

  constructor(private  service: BlogApiService) {
  }

  ngOnInit() {
    this.service.getListUtilisateur().subscribe(response =>
      this.utilisateurList = response);
  }

  public logger() {
    console.log(this.email);
    console.log(this.password);
    const filter = {email: this.email, password: this.password};
    const result = this.utilisateurList.filter(search, filter);
    console.log(result);
    if (result[0].password === this.password) {
      this.islog = true;
      /* this.utilisateurLogged.emit(result[0]);*/
      this.utilisateurLogged = result[0];
    } else {
      this.islog = false;
      this.display = true;
    }

    function search(user) {
      return Object.keys(this).every((key) => user[key] === this[key]);
    }

    console.log('La connexion est ' + this.islog);
    console.log(this.utilisateurLogged.pseudo + ' est connecté(e)');
  }


  public toggle() {
    this.display = !this.display;
  }
}
