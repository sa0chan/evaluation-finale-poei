import {Component, OnInit} from '@angular/core';
import {BlogApiService} from '../../../services/BlogApi.service';
import {NewUtilisateursModel} from '../../../models/Utilisateurs/NewUtilisateur.model';

@Component({
  selector: 'app-create-utilisateur',
  templateUrl: './create-utilisateur.component.html',
  styleUrls: ['./create-utilisateur.component.css']
})
export class CreateUtilisateurComponent implements OnInit {

  pseudo: string;
  email: string;
  password: string;
  passwordTest: string;
  display = false;
  isemailCorrect: boolean;

  constructor(private service: BlogApiService) {
  }

  ngOnInit() {
  }

  public enregistrer() {
    const body = new NewUtilisateursModel(this.pseudo, this.email, this.password);
    this.service.createUtilisateur(body).subscribe();
  }

  public toggle() {
    this.display = !this.display;
  }


  public validEmail(email) {
    const filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
    this.isemailCorrect = String(email).search(filter) !== -1;
    return this.isemailCorrect;
  }
}
