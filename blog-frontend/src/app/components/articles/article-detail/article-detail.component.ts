import {Component, OnInit} from '@angular/core';
import {BlogApiService} from '../../../services/BlogApi.service';
import {ActivatedRoute} from '@angular/router';
import {ArticleModel} from '../../../models/Article/Article.model';
import {UtilisateursModel} from '../../../models/Utilisateurs/Utilisateurs.model';
import {getIifeBody} from '@angular/compiler-cli/ngcc/src/host/esm5_host';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {

  article: ArticleModel;
  utilisateur: UtilisateursModel;

  constructor(private service: BlogApiService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getOneArticle(id).subscribe(response => {
      this.article = response;
      console.log(this.article);

      this.service.getOneUtilisateur(this.article.utilisateursId).subscribe(utilisateur =>
        this.utilisateur = utilisateur);
    });
  }


  public publierArticle() {
    const body = null;
    this.service.publierArticle(this.article.id, body).subscribe();
    // reload
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getOneArticle(id).subscribe(Newresponse => {
      this.article = Newresponse;
      console.log(this.article);
    });
  }
}
