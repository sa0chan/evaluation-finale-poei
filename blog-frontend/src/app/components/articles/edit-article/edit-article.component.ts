import {Component, OnInit} from '@angular/core';
import {BlogApiService} from '../../../services/BlogApi.service';
import {NewArticleModel} from '../../../models/Article/NewArticle.model';
import {ArticleModel} from '../../../models/Article/Article.model';
import {ActivatedRoute, ParamMap, PRIMARY_OUTLET, Router, UrlSegment, UrlSegmentGroup} from '@angular/router';
import * as url from 'url';
import {LocationStrategy} from '@angular/common';
import {state} from '@angular/animations';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.css']
})
export class EditArticleComponent implements OnInit {

  titre: string;
  resume: string;
  contenu: string;
  id: string;
  article: ArticleModel;
  utilisateursId: string;
  statutArticle: string;
  display = false;


  constructor(private service: BlogApiService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getOneArticle(id).subscribe(response => {
      this.article = response;
      this.utilisateursId = response.utilisateursId;
      this.statutArticle = response.statutArticles;
      this.titre = response.titre;
      this.resume = response.resume;
      this.contenu = response.contenu;
      this.id = response.id;
    });
  }

  public enregistrer() {
    if (this.resume.length < 50) {
      const body = new NewArticleModel( this.titre,  this.utilisateursId,  this.contenu, this.resume);
      console.log(body);
      this.service.editArticle(this.id, body).subscribe();
    }
  }

  public toggle() {
    this.display = !this.display;
  }


}
