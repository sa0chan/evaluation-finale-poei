import {Component, Input, OnInit, Output} from '@angular/core';
import {BlogApiService} from '../../../services/BlogApi.service';
import {ArticleModel} from '../../../models/Article/Article.model';
import {UtilisateursModel} from '../../../models/Utilisateurs/Utilisateurs.model';

@Component({
  selector: 'app-article-liste',
  templateUrl: './article-liste.component.html',
  styleUrls: ['./article-liste.component.css']
})
export class ArticleListeComponent implements OnInit {

  @Input()
  utilisateurLogged: UtilisateursModel;
  articleListe: ArticleModel[];
  id: string;
  article: string;


  constructor(private service: BlogApiService) {
  }

  ngOnInit() {
    this.articleListe = [];
    this.service.getListArticle().subscribe(response => {
      console.log(response);
      this.articleListe = response;
    });
  }

  public recupUtilisateurLogged(eventEmitted: UtilisateursModel){
    this.utilisateurLogged = eventEmitted ;
  }


}
