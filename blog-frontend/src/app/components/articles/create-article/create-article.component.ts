import {Component, OnInit} from '@angular/core';
import {BlogApiService} from '../../../services/BlogApi.service';
import {NewArticleModel} from '../../../models/Article/NewArticle.model';
import {UtilisateursModel} from '../../../models/Utilisateurs/Utilisateurs.model';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {
  titre: string;
  resume = '';
  contenu: string;
  idUtilisateur: string;
  utilisateurListe: UtilisateursModel[] = [];

  constructor(private service: BlogApiService) {
  }

  ngOnInit() {
    // recup des utilisateurs
    this.service.getListUtilisateur().subscribe(response => {
      this.utilisateurListe = response;
      console.log(this.utilisateurListe);
    });
  }


  public enregistrer() {
    if (this.resume.length < 50) {
      console.log(this.idUtilisateur);
      console.log(this.resume);
      console.log(this.contenu);
      console.log(this.titre);
      const body = new NewArticleModel(this.titre, this.idUtilisateur, this.resume, this.contenu,);
      console.log(body);
      this.service.createArticle(body).subscribe();
    }
  }
}
