export class ArticleModel {

  id: string;
  titre: string;
  contenu: string;
  resume: string;
  statutArticles: string;
  utilisateursId: string;

  constructor(id: string, utilisateursId: string, titre: string, contenu: string, resume: string, statutArticles: string) {
    this.id = id;
    this.titre = titre;
    this.contenu = contenu ;
    this.resume = resume;
    this.statutArticles = statutArticles;
    this.utilisateursId = utilisateursId;
  }

}
