export class NewArticleModel {

  titre: string;
  contenu: string;
  resume: string;
  utilisateursId: string;

  constructor(titre: string, utilisateursId: string, contenu: string, resume: string) {
    this.titre = titre;
    this.contenu = contenu ;
    this.resume = resume;
    this.utilisateursId = utilisateursId;
  }

}
