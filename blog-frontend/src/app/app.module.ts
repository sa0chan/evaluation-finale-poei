import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {BlogApiService} from './services/BlogApi.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {ArticleListeComponent} from './components/articles/article-liste/article-liste.component';
import {CreateArticleComponent} from './components/articles/create-article/create-article.component';
import {EditArticleComponent} from './components/articles/edit-article/edit-article.component';
import {ArticleDetailComponent} from './components/articles/article-detail/article-detail.component';
import {CreateUtilisateurComponent} from './components/utilisateurs/create-utilisateur/create-utilisateur.component';
import { LoginComponent } from './components/utilisateurs/login/login.component';

const appRoutes: Routes = [
  {path: 'articles', component: ArticleListeComponent},
  {path: 'create-articles', component: CreateArticleComponent},
  {path: 'edit-articles/:id', component: EditArticleComponent},
  {path: 'article-detail/:id', component: ArticleDetailComponent},
  {path: 'create-utilisateur', component: CreateUtilisateurComponent},
  {path: 'login', component: LoginComponent},



];

@NgModule({
  declarations: [
    AppComponent,
    ArticleListeComponent,
    CreateArticleComponent,
    EditArticleComponent,
    ArticleDetailComponent,
    CreateUtilisateurComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule
  ],
  providers: [BlogApiService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
