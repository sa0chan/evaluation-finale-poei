import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ArticleModel} from '../models/Article/Article.model';
import {NewArticleModel} from '../models/Article/NewArticle.model';
import {UtilisateursModel} from '../models/Utilisateurs/Utilisateurs.model';
import {NewUtilisateursModel} from '../models/Utilisateurs/NewUtilisateur.model';

@Injectable()
export class BlogApiService {
  constructor(private http: HttpClient) {
  }

  // Articles
  getListArticle(): Observable<ArticleModel[]> {
    return this.http.get<ArticleModel[]>(`http://localhost:8080/articles`);
  }

  createArticle(body): Observable<NewArticleModel> {
    return this.http.post<NewArticleModel>(`http://localhost:8080/articles`, body);
  }

  editArticle(id: string, body): Observable<ArticleModel> {
    return this.http.put<ArticleModel>(`http://localhost:8080/articles/${id}`, body);
  }


  getOneArticle(id: string): Observable<ArticleModel> {
    return this.http.get<ArticleModel>(`http://localhost:8080/articles/${id}`);
  }

  publierArticle(id: string, body): Observable<ArticleModel> {
    return this.http.post<ArticleModel>(`http://localhost:8080/articles/publication/${id}`, body);
  }

  // utilisateurs

  getListUtilisateur(): Observable<UtilisateursModel[]> {
    return this.http.get<UtilisateursModel[]>(`http://localhost:8080/utilisateurs`);
  }

  createUtilisateur(body): Observable<NewUtilisateursModel> {
    return this.http.post<NewUtilisateursModel>(`http://localhost:8080/utilisateurs`, body);
  }

  getOneUtilisateur(id): Observable<UtilisateursModel> {
    return this.http.get<UtilisateursModel>(`http://localhost:8080/utilisateurs/${id}`);
  }
}
